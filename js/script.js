$(document).ready(function () {
    var windowWidth = $(window).outerWidth();
    Scrollbar.initAll();

    $('.js-size-table').on('click', function(e) {
        let target = $(e.target)
        if (target.closest('.js-size-row').length) {
            target.closest('.js-size-row').addClass('active').siblings().removeClass('active');
        }
    });

    //Select - выберите размер
    $(".size_select_wrapper").click(function () {
        var select = $(".size_select");
        if (!select.hasClass('active')) {
            select.addClass('active');
            $('.size_options').slideDown(400);
            document.addEventListener('click', closeSizeList);
        } else {
            select.removeClass('active');
            $('.size_options').slideUp(400);
            document.removeEventListener('click', closeSizeList);
        }
    });
    function closeSizeList(e) {
        console.log("Click");
        if ($(".size_select").hasClass('active') && !e.target.closest('.size_select') && !e.target.closest('.size_options')) {
            $(".size_select").removeClass('active');
            $('.size_options').slideUp(400);
            document.removeEventListener('click', closeSizeList);
        }
    }
    $('.size_options').find('li:not(.not-available)').click(function () {
        var val = $(this).html();
        $('.size_select_placeholder').html(val);
        $('.size_select_wrapper').removeClass('error');
        $('.js-size-warning').slideUp(400);
        $(".size_select").removeClass('active');
        $('.size_options').slideUp(400);
        document.removeEventListener('click', closeSizeList);
    });
    $('.addToCart').click(function (e) {
        e.preventDefault();
        var value = $('.size_select_placeholder').html();
        if (value === 'Выберите размер') {
            $('.size_select_wrapper').addClass('error');
            $('.js-size-warning').slideDown(400);
            return false;
        }
    });
//serch
    $('.header__user-nav').find('.search').find('input[type="search"]').focus(function () {
        $('.search_hint').slideDown(300).addClass('active');
    })
    $(document).on('click', function(e) {
        if ($('.search_hint').hasClass('active') && !$(e.target).closest('.search_hint').length) {
            $('.search_hint').slideUp(300).removeClass('active');
        }
    });
    // $('.header__user-nav').find('.search').find('input[type="search"]').focusout(function () {
    //     $('.search_hint').slideUp(300);
    // })
    $('.subscribe_form').find('input').focus(function (e) {
        $('.subscribe_form').find('.popap_mess').fadeIn(500);
    })
    $('.subscribe_form').find('input').focusout(function (e) {
        $('.subscribe_form').find('.popap_mess').fadeOut(500);
    })
    // Promo slider
    if ($('.js-promo-slider').length) {
        $('.js-promo-slider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            arrows: false,
        });
    }

    // Sertificats slider
    if ($('.js-certificat-slider').length) {
        $('.js-certificat-slider').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: false,
            asNavFor: '.js-certificat-thumb-slider',
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                    }
                }
            ],
        });
        $('.js-certificat-thumb-slider').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            arrows: false,
            swipe: false,
            infinite: false,
            focusOnSelect: true,
            asNavFor: '.js-certificat-slider',
        });
    }

    // Product slider
    if ($('.js-product-slider').length && $('.js-thumb-product-slider').length) {
        $('.js-product-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
            arrows: true,
            infinite: false,
			asNavFor: '.js-thumb-product-slider',
		});
		$('.js-thumb-product-slider').slick({
			slidesToShow: 4,
            slidesToScroll: 1,
            arrows: false,
            infinite: false,
            vertical: true,
            swipe: false,
            focusOnSelect: true,
            asNavFor: '.js-product-slider',
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 3,
                        vertical: false,
                    },
                }
            ],
		});
    }

    // Big foto popup
    $('.aside').find('.slide').click(function (e) {
        e.preventDefault();

        var index = $(this).index();
        var top = $('.big_foto .foto').eq(index).position().top + $('.big_foto_wrapper').scrollTop();
        $(".big_foto_wrapper").scrollTop(top);
    })
    $('.big_foto_wrapper').scroll(function () {
        $(".big_foto").find('.foto').each(function (i, e) {
            var top = $(e).position().top + $('.big_foto_wrapper').scrollTop();
            var test = top + $(e).outerHeight() / 2;
            var w_scroll = $('.big_foto_wrapper').scrollTop();
            if (w_scroll > top && w_scroll < test || w_scroll === top) {
                $('.aside').find('.action').removeClass('action')
                $('.aside').find('.slide').eq(i).addClass('action');
            }
        });
    })
    $('.big_foto_wrapper .close_foto').click(function (e) {
        e.preventDefault();
        $('.foto_visible').removeClass('foto_visible');
        $('.big_foto_wrapper').scrollTop(0);
        $('body').removeClass('overflow');
    });

    $('.js-big-foto').click(function () {
        var index=$(this).closest('.slick-slide').index();
        $('.big_foto_wrapper').addClass('foto_visible');
        var top=$('.big_foto_wrapper .foto').eq(index).position().top;
        $('.big_foto_wrapper').scrollTop(top);
        $('body').addClass('overflow');
    });

    //Анимационный разворот стрелочки в карточке товара
    $(".product_info .detail_label").click(function () {
        $(".product_info .detail").toggle(500);
        $(".product_info .detail_label .arrow").toggleClass("rotate");
        return false;
    });

    //Анимационный разворот стрелочки в FAQ
    $(".info_block").click(function () {
        if ($(this).find('.detail_info').is(":visible")) {
            $(this).find('.detail_info').slideUp(600);
            $(this).removeClass('open_info');
        }
        else {
            $(this).find('.detail_info').slideDown(600);
            $(this).addClass('open_info');
        }

        return false;
    });

    //Анимационный разворот стрелочки в Купон
    $(".js-coupon").click(function () {
        $(this).toggleClass('active');
        if (!$(this).hasClass('active')) {
            $(".coupon .order_content").slideUp();
        } else {
            $(".coupon .order_content").slideDown();
        }
        $(this).toggleClass("rotate");
        return false;
    });

    //Вывод поиска
    $(".js-search").click(function () {
        $(".header .search input").show(500);
    });
    $('.header').find('.search').click(function (e) {
        e.stopPropagation();
    });
    $('body').click(function () {
        $(".header .search input").hide(500);
    });

    // $('.js-show-navigation-menu').mouseenter(function() {
    //     $(this).find('.js-navigation-menu').addClass('active').slideDown(400);
    //     // $(this).siblings('.js-overlay').fadeIn(400);
    // });
    $('.js-show-navigation-menu').click(function(e) {
        e.stopPropagation();
        var menu = $(this).siblings('.js-navigation-menu');
        if (menu.hasClass('active')) {
            menu.slideUp(400);
        } else {
            menu.slideDown(400);
            $('.js-navigation-menu.active').removeClass('active').slideUp(400);
        }
        menu.toggleClass('active');
    });
    $(document).click(function(e) {
        var activeMenu = $(this).find('.js-navigation-menu.active');

        if (activeMenu.length && !$(e.target).closest('.js-navigation-menu.active').length) {
            console.log("Body click");
            activeMenu.removeClass('active').slideUp(400);
        }
    })
    // $('.js-show-navigation-menu').mouseleave(function() {
    //     $(this).find('.js-navigation-menu').removeClass('active').slideUp(400);
    //     // $(this).siblings('.js-overlay').fadeIn(400);
    // });
    $('.js-close-navigation-menu').click(function(e) {
        e.preventDefault();
        $(this).parent().removeClass('active').slideUp(400);
        // $(this).parent().parent().find('.js-overlay').fadeOut(400);
    });

    //Переключение форм Логин-Регистрация
    $('.js-tabs').on('click', function(e) {
        var el = $(e.target);
        if (el.hasClass('js-tabs-toggle') && !el.hasClass('active')) {
            el.addClass('active').siblings().removeClass('active');
            $(this).find('.js-tabs-content').removeClass('active').filter('#' + el.data('type')).addClass('active');
        }
    });
    // Показываем форму восстановления пароля
    $('.js-forgot-pass').on('click', function(e) {
        e.preventDefault();
        $('.js-tabs-auth').hide();
        $('.js-restore-password').show();
    });
    // Возвращаемся с восстановления пароля на авторизацию
    $('.js-back-to-auth').on('click', function(e) {
        e.preventDefault();
        $('.js-tabs-auth').show();
        $('.js-restore-password').hide();
    });

    $('#rating').find('a').click(function (e) {
        e.preventDefault();
        $('#rating').find('.action').removeClass('action');
        $(this).addClass('action');
    });
    // Модалки с вкладками
    $('.js-size-popup-toggle').click(function (e) {
        e.preventDefault();
        var popup = $('.popap_size'),
            idBlock = '#' + $(this).data('type');
        popup.find('.button_line .active').removeClass('active');
        $(this).addClass('active');
        popup.find('.js-popup-size-section').removeClass('visible_block').siblings(idBlock).addClass('visible_block');
    });
    $('.close_popap').click(function () {
        $('.popap_size').fadeOut(500);
        $('body').removeClass('overflow');
        setTimeout(function () {
            $('.popap_fon').fadeOut(500);
        }, 700);
    });
    $('.js-open-size-popup').click(function (e) {
        e.preventDefault();
        var type = $(this).data('type');
        $('.popap_fon').fadeIn(500);
        $('body').addClass('overflow');
        setTimeout(function () {
            var popup = $('.popap_size');
            popup.find('.button_line a').removeClass('active').siblings('[data-type="' + type + '"]').addClass('active');
            popup.find('.js-popup-size-section').removeClass('visible_block').siblings('#' + type).addClass('visible_block');
            popup.fadeIn(500);
        }, 700);
    });
    /***************************************************************/



    /******************************************************************/

//     if($('.catalog_item_img').length>0){
//         var array = [];

// // Проход по всем элементам с классом daily-person-id
// // с помощью jQuery each.
//         $(".catalog_item_img").each(function (index, el){
//             // Для каждого элемента сохраняем значение в personsIdsArray,
//             // если значение есть.
//             var v  = $(el).find('img').outerHeight();
//             if (v) array.push(v);
//             // console.log(array);
//         });

//         var min = getMinValue(array);
//         $('.catalog_item_img').css('max-height',min);
//         function getMinValue(array){
//             var min = array[0];
//             for (var i = 0; i < array.length; i++) {
//                 if (min > array[i]) min = array[i];
//             }
//             return min;
//         }
//     }

    // Скрываем предупреждение Cookie
    $(document).on('click', '.js-warning-button', function() {
        $('.js-warning').slideUp();
        setTimeout(function() {
            instHeighMobileMenu();
        }, 400);
    });

    $('.js-delete-of-cart').click(function(e) {
        e.preventDefault();
        var productBlock = $(this).parents('.cart_product');
        $(this).parents('.cart_item_wrapper').css('display', 'none');
        $(this).parents('.cart_item_wrapper').prev('.delet_product').css('display', 'flex');
        setTimeout(function() {
            productBlock.remove();
        }, 3000);
        // $.post('/delete', {product: '00001'})
        //     .done(function() {

        //     });
    });
    // Селект выбора страны
    if ($('.js-country-select').length) {
        var formatStateCountry = function (state) {
            if (!state.id) {
                return state.text;
            }

            var imageUrl = 'img/flags/' + state.element.value.toLowerCase() + '.png';
            var $state = $('<span><img src="' + imageUrl + '" class="img-flag">' + state.text + '</span>');

            return $state;
        };

        $('.js-country-select').each(function() {
            var $select = $(this);

            $select.select2({
                placeholder: $select.data('placeholder'),
                templateResult: formatStateCountry,
                minimumResultsForSearch: -1,
                width: 'resolve',
                dropdownParent: $select.closest('.main-select')
            });

            if ($select.parent().siblings('.js-order-flag').length) {
                $select.on('change', function () {
                    var imageUrl = 'img/flags/' + this.value.toLowerCase() + '.png';
                    $('.js-order-flag').attr('src', imageUrl);
                });
            }

            $select.on('select2:open', function (e) {
                e.preventDefault();
                Scrollbar.init(this.parentElement.querySelector('.select2-results'), {
                    damping: 0.05,
                });
            });

            $select.on('select2:close', function (e) {
                e.preventDefault();
                Scrollbar.destroy(this.parentElement.querySelector('.select2-results'));
            });
        });
    }
    // Селект выбора валюты
    if ($('.js-currency-select').length) {
        var formatStateCurrency = function (state) {
            if (!state.id) {
                return state.text;
            }

            var $state = $('<span><i class="fa fa-' + state.element.value.toLowerCase() + '" aria-hidden="true"></i>&nbsp;' + state.text + '</span>');

            return $state;
        };

        $('.js-currency-select').each(function() {
            var $currencySelect = $(this);

            $currencySelect.select2({
                placeholder: $currencySelect.data('placeholder'),
                templateResult: formatStateCurrency,
                minimumResultsForSearch: -1,
                width: 'resolve',
                dropdownParent: $currencySelect.closest('.main-select')
            });

            $currencySelect.on('select2:open', function (e) {
                e.preventDefault();
                Scrollbar.init(this.parentElement.querySelector('.select2-results'), {
                    damping: 0.05,
                });
            });

            $currencySelect.on('select2:close', function (e) {
                e.preventDefault();
                Scrollbar.destroy(this.parentElement.querySelector('.select2-results'));
            });
        });
    }

    // Обычные селекты
    if ($('.js-select').length) {
        $('.js-select').each(function() {
            var select = $(this);
            select.select2({
                placeholder: select.data('placeholder'),
                minimumResultsForSearch: -1,
                width: 'resolve',
                dropdownParent: select.closest('.main-select')
            });

            select.on('select2:open', function (e) {
                e.preventDefault();
                Scrollbar.init(this.parentElement.querySelector('.select2-results'), {
                    damping: 0.05,
                });
            });

            select.on('select2:close', function (e) {
                e.preventDefault();
                Scrollbar.destroy(this.parentElement.querySelector('.select2-results'));
            });
        });
    }

    var toggleOrderBlock = function(selector) {
        $(selector).find('.js-order-block-change').toggleClass('active');
        $(selector).find('.js-order-block-apply').toggleClass('active');
    };

    $('.js-country-change').on('click', function(e) {
        e.preventDefault();
        document.querySelector('.js-id-country').value = '';
        toggleOrderBlock('.js-delivery-block');
    });

    $('.js-country-apply').on('click', function (e) {
        e.preventDefault();
        $('.js-country-result').text( this.parentElement.querySelector('.js-current-country').value);
        toggleOrderBlock('.js-delivery-block');
    });

    $('.js-email-change').on('click', function(e) {
        e.preventDefault();
        toggleOrderBlock('.js-email-block');
    });

    $('.js-email-apply').on('click', function (e) {
        e.preventDefault();
        var emailInput = this.parentElement.querySelector('.js-email-input');
        $('.js-email-result').text(emailInput.value);
        toggleOrderBlock('.js-email-block');
    });

    $('.js-address-apply').on('click', applyAddress);

    function applyAddress(e) {
        e.preventDefault();
        let form = document.querySelector('.js-order-form'),
            valid = true,
            requirInputs = Array.from(form.elements).filter(el => el.required);

        for (let input of requirInputs) {
            if (input.checkValidity() === false) {
                valid = false;
            }
        }

        if (valid) {
            console.log("Отправляем данные!!!");
            toggleOrderBlock('.js-address-block');
        }
    }

    $('.js-address-change').on('click', function(e) {
        e.preventDefault();
        toggleOrderBlock('.js-address-block');
    });

    // Video
    if ($('.js-video').length) {
        $('.js-video').magnificPopup({
            type: 'iframe',
            preload: false,
        });
        $('.js-video').click(function(e) {
            e.stopPropagation();
        });
    }

    // Catalog filter
    if ($('.js-filter').length) {
        $('.js-filter-select').on('click', function() {
            var filterSelect = $(this);
            if (!filterSelect.hasClass('active')) {
                filterSelect.addClass('active').siblings().removeClass('active');
                $('.js-filter-list').slideUp(300);
                filterSelect.find('.js-filter-list').slideDown(300);
            } else {
                filterSelect.removeClass('active');
                filterSelect.find('.js-filter-list').slideUp(300);
            }
        });

        $('.js-filter-select label').on('click', function(e) {
            e.stopPropagation();
        })

        $('.js-filter-item').on('change', function(e) {
            // Запрос на фильтрацию
        });

        $(document).on('click', function(e) {
            if ($('.js-filter-select.active') && !$(e.target).closest('.js-filter-select').length) {
                $('.js-filter-select').removeClass('active');
                $('.js-filter-list').slideUp(300);
            }
        });
    }

    // Sticky block
    if ($('.js-sticky-block').length && $('.js-sticky-stop').length) {
        // var win_w = $(window).outerWidth()
        var top = $('.js-sticky-block').offset().top;
        $(window).scroll(function() {
            if (document.documentElement.clientWidth >= 1024) {
                var scroll_w = $('body,html').scrollTop();
                var stickyBlock = $('.js-sticky-block');
                var stop = $('.js-sticky-stop').offset().top + $('.js-sticky-stop').outerHeight();
                var test = scroll_w + stickyBlock.outerHeight();
                if (stop <= test) {
                    var top_stop = stop - stickyBlock.outerHeight();
                    var left = stickyBlock.offset().left;
                    var width = stickyBlock.outerWidth();
                    stickyBlock.css({
                        position: "absolute",
                        top: top_stop,
                        left: left,
                        width: width
                    })
                } else {
                    if (scroll_w >= top) {
                        var left = stickyBlock.offset().left;
                        var width = stickyBlock.outerWidth();
                        stickyBlock.css({
                            position: "fixed",
                            top: 0,
                            left: left,
                            width: width
                        })
                    } else {
                        stickyBlock.removeAttr('style');
                    }
                }
            } else {
                $('.js-sticky-block').removeAttr('style');
            }
        });

        $(window).resize(function() {
            var top = $('.js-sticky-block').offset().top;
            if (window.outerWidth < 1024) {
                $('.js-sticky-block').removeAttr('style');
            } else {
                var scroll_w = $('body,html').scrollTop();
                var stickyBlock = $('.js-sticky-block');
                var stop = $('.js-sticky-stop').offset().top + $('.js-sticky-stop').outerHeight();
                var test = scroll_w + stickyBlock.outerHeight();
                if (stop <= test) {
                    var top_stop = stop - stickyBlock.outerHeight();
                    var left = $('.js-sticky-stop').offset().left + $('.js-sticky-stop').innerWidth() + 12;
                    var width = stickyBlock.outerWidth();
                    stickyBlock.css({
                        position: "absolute",
                        top: top_stop,
                        left: left,
                        width: width
                    })
                } else {
                    if (scroll_w >= top) {
                        var left = $('.js-sticky-stop').offset().left + $('.js-sticky-stop').innerWidth() + 12;
                        var width = stickyBlock.outerWidth();
                        stickyBlock.css({
                            position: "fixed",
                            top: 0,
                            left: left,
                            width: width
                        })
                    }
                }
            }
        })
    }

    // Mobile menu
    $('.js-menu-button').on('click', function(e) {
        e.preventDefault();
        $(this).toggleClass('active');
        $('.js-mobile-menu').toggleClass('active');
        $('body').toggleClass('overflow');
    });

    $('.js-navigation-el').on('click', function(e) {
        if (windowWidth < 1024 && !$(e.target).closest('.js-navigation-sub').length) {
            e.preventDefault();
            
            var navigationEl = $(this);
            if (navigationEl.hasClass('active')) {
                navigationEl.find('.js-navigation-sub').slideUp(300);
            } else {
                $('.js-navigation-el').removeClass('active');
                $('.js-navigation-sub').slideUp(300);
                navigationEl.find('.js-navigation-sub').slideDown(300);
            }
            navigationEl.toggleClass('active');
        }
    });
    // Favorite button
    $('.js-favorite').on('click', function() {
        // Нужен запрос на добавление товара в избранное
        $(this).toggleClass('active');
    });
    // Ajax popup
    if ($('.js-popup').length) {
        $('.js-popup').magnificPopup({
            type: 'ajax',
            alignTop: true,
        });
    }

    if ($('.js-instashop-slider').length) {
        $('.js-instashop-slider').slick({
            slidesToShow: 4,
            rows: 2,
            slidesPerRow: 4,
            infinite: false,
            arrows: false,
            variableWidth: true,
            responsive: [
                {
                    breakpoint: 1023,
                    settings: {
                        rows: 1,
                        slidesPerRow: 1,
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    },
                },
                {
                    breakpoint: 767,
                    settings: {
                        rows: 1,
                        slidesPerRow: 1,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    },
                },
            ]
        });
    }

    // Показываем пароль
    $('.js-show-password').on('click', function(e) {
        e.preventDefault();
        $(this).siblings('input').attr('type', function (index, val) {
            return val === 'password' ? 'text' : 'password';
        });
    });

    // Уадление избранного товара
    $('.js-delete-favorite').on('click', function(e) {
        e.preventDefault();
        // Запрос на удаление товара
        $(e.target).parent().remove();
    });

    var instHeighMobileMenu = function() {
        if ($(window).outerWidth() < 1024) {
            var headerHeight = $('.header').outerHeight();
            var headerTop = $('.header').offset().top;
            var clientHeight = document.documentElement.clientHeight;

            $('.js-mobile-menu').css('max-height', clientHeight - headerHeight - headerTop);
        } else {
            $('.js-mobile-menu').removeAttr('style');
        }
    };

    $('.js-toggle-brand-navigation').on('click', function(e) {
        var brandLists = $('.js-brands-list'),
            brandsNavigations = $('.js-brand-navigation');

        brandsNavigations.toggleClass('active');
        brandLists.removeClass('active').filter('#' + brandsNavigations.filter('.active').data('block')).addClass('active');
        $(e.target).text(brandsNavigations.filter('.active').data('lang'));
    });

    if ($('.js-current-country').length) {
        var countryScroll = Scrollbar.init(document.querySelector('.js-dropdown-country-scroll'));
        var curCountry = '';
        $('.js-current-country').on('input', function(e) {
            if (e.target.value.trim().length > 2) {
                $.get('js/data/country.json', {data: e.target.value.trim()}).done(
                    function(res) {
                        if (res.status === 'ok') {
                            Array.from(countryScroll.contentEl.children).forEach(function(item) {
                                $(item).off('click');
                                item.remove();
                            });
                            countryScroll.contentEl.append(createDropdownList(res.data, createCountry));
                            countryScroll.update();
                            $('.js-country-dropdown').addClass('active');
                            $('.js-country').on('click', selectCountry);
                            curCountry = '';
                        }
                    }
                );
            } else {
                if ($('.js-country-dropdown').hasClass('active')) {
                    $('.js-country-dropdown').removeClass('active');
                    Array.from(countryScroll.contentEl.children).forEach(function(item) {
                        $(item).off('click');
                        item.remove();
                    });
                    curCountry = '';
                }
            }
        });

        $(document).on('click', function(e) {
            if ($('.js-country-dropdown').hasClass('active') &&
                !curCountry && countryScroll.contentEl.children.length &&
                !$(e.target).closest('.js-current-country').length &&
                !$(e.target).closest('.js-country-dropdown').length) {
                $('.js-country-dropdown').removeClass('active');
                countryScroll.contentEl.children[0].click();
            }
        });

        $('.js-current-country').on('keydown', function(e) {
            if (!curCountry && countryScroll.contentEl.children.length && e.keycode === 13) {
                $('.js-country-dropdown').removeClass('active');
                countryScroll.contentEl.children[0].click();
            }
        });

        function createCountry(data) {
            var countryDiv = document.createElement('div');
    
            countryDiv.classList.add('dropdown__item');
            countryDiv.classList.add('js-country');
            countryDiv.setAttribute('data-id', data.ID);
            countryDiv.setAttribute('data-flag', data.FLAG);
            countryDiv.innerText = data.COUNTRY;

            return countryDiv;
        }

        function selectCountry(e) {
            var countryId = e.target.dataset.id;
            var flagUrl = e.target.dataset.flag;
            var countryString = e.target.innerText;

            curCountry = e.target.innerText;

            $('.js-current-country').val(countryString);
            $('.js-id-country').val(countryId);
            $('.js-order-flag').each(function(i, item) {
                item.src = flagUrl;
            });
            $('.js-country-dropdown').removeClass('active');
        }
    }

    if ($('.js-current-city').length) {
        var cityScroll = Scrollbar.init(document.querySelector('.js-dropdown-city-scroll'));
        var curCity = '';
        $('.js-current-city').on('input', function(e) {
            if (e.target.value.trim().length > 2) {
                $('.js-id-city').val('');
                $.get('js/data/city.json', {data: e.target.value.trim()}).done(
                    function(res) {
                        if (res.status === 'ok') {
                            Array.from(cityScroll.contentEl.children).forEach(function(item) {
                                $(item).off('click');
                                item.remove();
                            });
                            cityScroll.contentEl.append(createDropdownList(res.data, createCity));
                            cityScroll.update();
                            $('.js-city-dropdown').addClass('active');
                            $('.js-city').on('click', selectCity);
                            curCity = '';
                        }
                    }
                );
            } else {
                if ($('.js-city-dropdown').hasClass('active')) {
                    $('.js-city-dropdown').removeClass('active');
                    Array.from(cityScroll.contentEl.children).forEach(function(item) {
                        $(item).off('click');
                        item.remove();
                    });
                    curCity = '';
                }
            }
        });

        $(document).on('click', function(e) {
            if ($('.js-city-dropdown').hasClass('active') &&
                !curCity && cityScroll.contentEl.children.length &&
                !$(e.target).closest('.js-current-city').length &&
                !$(e.target).closest('.js-city-dropdown').length) {
                $('.js-city-dropdown').removeClass('active');
                cityScroll.contentEl.children[0].click();
            }
        });

        $('.js-current-city').on('keydown', function(e) {
            if (!curCity && cityScroll.contentEl.children.length && e.keycode === 13) {
                $('.js-city-dropdown').removeClass('active');
                cityScroll.contentEl.children[0].click();
            }
        });

        function createCity(data) {
            var cityDiv = document.createElement('div');
    
            cityDiv.classList.add('dropdown__item');
            cityDiv.classList.add('js-city');
            cityDiv.setAttribute('data-id', data.ID);
            cityDiv.innerText = data.CITY;

            return cityDiv;
        }

        function selectCity(e) {
            var cityId = e.target.dataset.id;
            var cityString = e.target.innerText;

            curCity = e.target.innerText;
            console.log('Click');
            $('.js-current-city').val(cityString);
            $('.js-id-city').val(cityId);
            $('.js-city-dropdown').removeClass('active');
        }
    }

    function createDropdownList(data, callback) {
        var fragment = document.createDocumentFragment();

        data.forEach(function(item) {
            fragment.append(callback(item));
        });

        return fragment;
    }

    addMaskOnInput();

    validateForm();

    $(document).ready(instHeighMobileMenu);

    $(window).resize(instHeighMobileMenu);
});

function addMaskOnInput() {
    $('.js-tel').inputmask('+9 (999) 999-99-99');
    $('.js-email').inputmask({ alias: "email" });
}

function validateForm() {
    $('.js-require-input').on('invalid', function(e) {
        e.preventDefault();
        if (this.validity.valueMissing || this.validity.typeMismatch) {
            $(this).parent().addClass('error').find('.js-field-warning').slideDown(300);
        }
    });

    $('.js-require-input').on('input', function() {
        if ($(this).val() && $(this).parent().hasClass('error')) {
            $(this).parent().removeClass('error').find('.js-field-warning').slideUp(300);
        }
    });
}
