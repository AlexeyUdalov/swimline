'use strict';

$(document).ready(function () {
	var arrPage = [
		{ label: 'Главная', value: 'index' },
		{ label: 'О нас', value: 'about' },
		{ label: 'Статьи', value: 'articles' },
		{ label: 'Статья', value: 'article' },
		{ label: 'Бренды', value: 'brands' },
		{ label: 'Корзина', value: 'cart' },
		{ label: 'Пустая корзина', value: 'cart_empty' },
		{ label: 'Каталог', value: 'catalog' },
		{ label: 'Сертификаты', value: 'certification' },
		{ label: 'Контакты', value: 'contacts' },
		{ label: 'Информация', value: 'info' },
		{ label: 'Инстаграм', value: 'instashop' },
		{ label: 'ЛК', value: 'LK' },
		{ label: 'ЛК-добавление адреса', value: 'LK_add_addresses' },
		{ label: 'ЛК-добавление карты', value: 'LK_add_payment' },
		{ label: 'ЛК-адреса', value: 'LK_addresses' },
		{ label: 'ЛК-изменение пароля', value: 'LK_change_Password' },
		{ label: 'ЛК-заказы', value: 'LK_orders' },
		{ label: 'ЛК-заказы (пустая)', value: 'LK_orders_empty' },
		{ label: 'ЛК-информация о заказе', value: 'LK_orders_cart' },
		{ label: 'ЛК-редактирование персональных данных', value: 'LK_personal' },
		{ label: 'ЛК-способы оплаты', value: 'LK_payment' },
		{ label: 'ЛК-способы оплаты(пустая)', value: 'LK_payment_empty' },
		{ label: 'ЛК-соцсети', value: 'LK_soc' },
		{ label: 'ЛК-соцсети(пустая)', value: 'LK_soc_empty' },
		{ label: 'Вход', value: 'login' },
		{ label: 'Оформление заказа неавторизованным пользователем', value: 'order_not_auth' },
		{ label: 'Оформление заказа', value: 'order' },
		{ label: 'Заказ оформлен', value: 'order_finish' },
		{ label: 'Карточка товара', value: 'product' },
		{ label: 'Отзывы', value: 'reviews' },
		{ label: 'Страница поиска', value: 'search' },
		{ label: 'Карта сайта', value: 'sitemap' },
		{ label: 'Избранное', value: 'wishlist' },

		{ label: 'Вверх', value: '#' }
	];
	var textColor = "white",
	    bgColor = "#343434";

	var $list = $('<ol id="pages2342"></ol>')
	$list.appendTo('body').css({
		'position': 'fixed',
		'left': -210,
		'width': 220,
		'top': '50%',
		'max-height': '100%',
		'overflow': 'auto',
		'margin': 0,
		'padding': '20px',
		'border': '1px solid '+textColor,
		'border-left': 0,
		'background': bgColor,
		'zIndex': 54512, 'fontSize': 14,
		'color': textColor,
		'fontFamily': 'Arial, sans-serif',
		'lineHeight': '20px',
		'opacity': '0.6',
		'box-sizing': 'border-box',
	});

	for (var i = 0; i < arrPage.length; i++) {
		$list.append('<li><a ' + (arrPage[i].className? "class='js-pjax '"+arrPage[i].className:"class='js-pjax'") + ' href="' + arrPage[i].value + '.html">' + arrPage[i].label + '</a></li>');
	}
	$('li', $list).css({
		'fontSize': 12,
		'color': textColor
	});

	$('a', $list).css({
		'display': 'inline-block',
		'width': '100%',
		'fontSize': 14,
		'color': textColor,
		'text-decoration': 'none'
	});

	$('li:last', $list).prepend('^').append('^').css({
		'fontWeight': 'bold',
		'listStyle': 'none',
		'textAlign': 'center'
	})
	.find('a')
	.attr('href', '#')
	.css({
		'width': 'auto'
	});

	$('<li><b id="arrow">&raquo;</b></li>').appendTo($list).css({
		'position': 'absolute',
		'top': '50%', 'right': 2,
		'height': 12,
		'margin-top': -12,
		'listStyle': 'none'
	});

	$list.css({
		marginTop: '-'+($list.outerHeight() / 2)+'px'
	})

	$('#arrow').css({ 'fontSize': 12, 'color': textColor });
	$list.hover(function () {
		$(this).css({ 'left': 0, 'opacity': '1' });
	}, function () {
		$(this).css({ 'left': -210, 'opacity': '0.6' });
	});
	$('a', $list).hover(function () {
		$(this).css('color', 'orange');
	}, function () {
		$(this).css('color', textColor);
	});
});
